﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour
{
    //Rows
    public int rows;
    //Columns
    public int cols;
    //Width of the room
    private float roomWidth = 50.0f;
    //Height of the room
    private float roomHeight = 50.0f;
    //Room Game Object List
    public List<GameObject> roomPrefab;
    public int seed;
    //Room Grid
    private Room[,] grid;
    //enum list of map types
    public enum mapTypes
    {
        mapOfTheDay,
        seedMap,
        randomMap
    }

    public mapTypes mapSelection;
    void Start()
    {
        GenerateMap();
    }
    public GameObject RandomRoomPrefab()
    {
        return roomPrefab[Random.Range(0, roomPrefab.Count)];
    }
    //Generate Map
    public void GenerateMap()
    {
        switch (mapSelection)
        {
            case mapTypes.mapOfTheDay:
            {
                DateTime dt = DateTime.Now;

                break;
            }
            case mapTypes.randomMap:
            {
                break;
            }
            case mapTypes.seedMap:
            {
                UnityEngine.Random.InitState(seed);
                break;
            }
        }
        //Grid
        grid = new Room[cols, rows];

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                CreateTile(newPosition, i, j);
                
            }
        }

    }

    void CreateTile(Vector3 position, int i, int j)
    {
        GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), position, Quaternion.identity) as GameObject;
    }
    //Operating doors
    void OperateDoors (Room room, int i, int j)
    {
        //****North and South Door****
        if (i == 0)
        {
            room.doorNorth.SetActive(false);
        }
        else if (i == rows - 1)
        {
            room.doorSouth.SetActive(false);
        }
        else
        {
            room.doorNorth.SetActive(false);
            room.doorSouth.SetActive(false);
        }
        //****North and South Door****
        //****East and West Door****
        if (j == 0)
        {
            room.doorEast.SetActive(false);
        }
        else if (j == rows - 1)
        {
            room.doorWest.SetActive(false);
        }
        else
        {
            room.doorEast.SetActive(false);
            room.doorWest.SetActive(false);
        }
        //****East and West Door****
    }
    //Map of the Day
    public int mapDayMthTime(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day;
    }

    public GameObject getRandomRoom()
    {

    }

}
